import axios from "axios";
import {BASE_URL} from '../tools/constante.js';
import {useState, useEffect} from "react";
import {useParams} from "react-router-dom";
import {Fragment} from "react";

const UpdateProduct = () => {
    
    const [messageLogin, setMessagelogin] = useState("");
    const [product, setProduct] = useState(null);
    const {id} = useParams();
    // Fonction pour modifier l'état messageLogin et vider celui-ci après 2 secondes
    const messageFn = (msg) => {
        setMessagelogin(msg);
        setTimeout(() => {
            setMessagelogin("");
        },2000);
    };
    //on met à jour dès que l'id change
    useEffect(() => {
        // Utilisation de useEffect pour récupérer le produit à partir de son id
        axios.get(`${BASE_URL}/products`,{id})
            .then(res => setProduct(res.data.data.result[0]))
            .catch(err => console.log(err));
    },[id]);
    
    const handleChange = (e) => {
        const {name, value} = e.target;
        //  on met à jour avec les nouvelles valeurs
        setProduct({...product, [name]: value});
    };
    
    const submit = (e) =>{
        e.preventDefault();
        
        if(product.name === "" || product.description === "" || product.price === ""){
            messageFn("Veuillez remplir tous les champs");
            return;
        }
       
        axios.put(`${BASE_URL}/products`,{...product})
        .then(res => messageFn(res.data.data.response))
        .catch(err => console.log(err));
    };
   
    return(
        <Fragment>
            {/*si product contient quelque chose alors on affiche la page de modif */}
            {product !== null && (
                <div className = "login contact" >
                    <h2>Mettre à jour votre produit</h2>
                    <div className="msgAlert"><h3>{messageLogin}</h3></div>
                    <form className="login-form" onSubmit={submit} >
                        <div className="form-item">
                            <label htmlFor="name">Nom</label>
                            <input type="text" name="name" placeholder="..." onChange={handleChange} value={product.name} maxLength="100"/>
                        </div>
                        <div className = "form-item">
                            <label htmlFor="description">Description</label>
                            <input type="text" name="description" placeholder="..." onChange={handleChange} value={product.description} maxLength="2000" />
                        </div>
                        <div className="form-item">
                            <label htmlFor="price">Allergènes</label>
                            <input type="text" name="price" placeholder="..." onChange={handleChange} value={product.price} maxLength="2000"/>
                        </div>
                        <button className="submit" type="submit">VALIDER</button>
                    </form>
                </div>
            )}
        </Fragment>
    );
};

export default UpdateProduct;