import axios from "axios";
import { BASE_URL } from "../tools/constante.js";
import { BASE_IMG } from "../tools/constante.js";
import { useState, useEffect, Fragment } from "react";

const Products = () => {
    const [products, setProducts] = useState([]);
    const [searchTerm, setSearchTerm] = useState("");
    const [currentPage, setCurrentPage] = useState(1);
    const itemsPerPage = 6;

    useEffect(() => {
        if (products.length === 0) {
            axios.get(`${BASE_URL}/products`)
                .then(res => setProducts(res.data.data.result))
                .catch(err => console.log(err));
        }
    }, [products]);

    const handleSearchChange = (e) => {
        setSearchTerm(e.target.value);
        setCurrentPage(1);
    };

    const filteredProducts = products.filter(product =>
        product.name.toLowerCase().includes(searchTerm.toLowerCase())
    );

    const indexOfLastItem = currentPage * itemsPerPage;
    const indexOfFirstItem = indexOfLastItem - itemsPerPage;
    const currentItems = filteredProducts.slice(indexOfFirstItem, indexOfLastItem);

    const totalPages = Math.ceil(filteredProducts.length / itemsPerPage);

    const handleClick = (event, page) => {
        event.preventDefault();
        setCurrentPage(page);
    };

    return (
        <Fragment>
            <div id="search">
                <input
                    type="text"
                    placeholder="Rechercher un produit"
                    value={searchTerm}
                    onChange={handleSearchChange}
                />
            </div>
            <div className="container_products">
                {currentItems.map((product, i) => (
                    <div key={i} className="product">
                        <div className="product-img">
                            <img src={`${BASE_IMG}/${product.url}`} alt={product.caption} />
                        </div>
                        <div className="product-info">
                            <h3>{product.name}</h3>
                            <p>{product.description}</p>
                            <h4>Allergènes</h4>
                            <p>{product.price}</p>
                        </div>
                    </div>
                ))}
            </div>
            <div className="pagination">
                {Array.from({ length: totalPages }, (_, i) => (
                    <button
                        key={i}
                        onClick={(e) => handleClick(e, i + 1)}
                        className={i + 1 === currentPage ? "active" : ""}
                    >
                        {i + 1}
                    </button>
                ))}
            </div>
        </Fragment>
    );
};

export default Products;
