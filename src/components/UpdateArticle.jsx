import axios from "axios";
import {BASE_URL} from '../tools/constante.js';
import {useState, useEffect} from "react";
import {useParams} from "react-router-dom";
import {Fragment} from "react";

const UpdateArticle = () => {
    
    const [messageLogin, setMessagelogin] = useState("");
    const [article, setArticle] = useState(null);
    const {id} = useParams();
    // Fonction pour modifier l'état messageLogin et vider celui-ci après 2 secondes
    const messageFn = (msg) => {
        setMessagelogin(msg);
        setTimeout(() => {
            setMessagelogin("");
        },2000);
    };
    // Utilisation de useEffect pour récupérer l'article à partir de son id
    useEffect(() => {
        // on envoie une requête avec l'id de l'article concerné
        axios.get(`${BASE_URL}/articles`,{id})
            .then(res => setArticle(res.data.data.result[0]))
            .catch(err => console.log(err));
    },[id]);
    
    const handleChange = (e) => {
        const {name, value} = e.target;
        //  on met à jour avec les nouvelles valeurs
        setArticle({...article, [name]: value});
    };
    
    const submit = (e) =>{
        e.preventDefault();
        
        axios.put(`${BASE_URL}/articles`,{...article})
        .then(res => messageFn(res.data.data.response))
        .catch(err => console.log(err));
    };
   
    return(
        <Fragment>
            {/*si product contient quelque chose alors on affiche la page de modif */}
            {article !== null && (
                <div className = "login contact" >
                    <h2>Mettre à jour votre article</h2>
                    <div className="msgAlert"><h3>{messageLogin}</h3></div>
                    <form className="login-form" onSubmit={submit} >
                        <div className="form-item">
                            <label htmlFor="title">Titre</label>
                            <input type="text" name="title" placeholder="..." onChange={handleChange} value={article.title} maxLength="100"/>
                        </div>
                        <div className = "form-item">
                            <label htmlFor="description">Description</label>
                            <textarea style={{ whiteSpace: 'pre-line' }} name="description" placeholder="..." onChange={handleChange} value={article.description} maxLength="2000" />
                        </div>
                        <button className="submit" type="submit">VALIDER</button>
                    </form>
                </div>
            )}
        </Fragment>
    );
};

export default UpdateArticle;