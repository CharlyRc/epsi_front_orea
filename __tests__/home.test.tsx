import { render, screen } from '@testing-library/react';
import { describe, it, expect } from 'vitest';
import React from 'react';
import { BrowserRouter as Router } from 'react-router-dom'; // Importer BrowserRouter
import Home from '../src/components/Home'; // Assurez-vous que le chemin est correct

describe('Home', () => {
  it('devrait afficher le message principal', async () => {
    render(
      <Router>
        <Home />
      </Router>
    );
    
    const mainMessage = await screen.findByText(/Pensez à passer commande une semaine avant votre évènement\./i);
    
    expect(mainMessage).toBeInTheDocument();
  });
});